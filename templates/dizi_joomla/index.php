<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$doc = Jfactory::getDocument();


// Add javascript

$doc->addScript('templates/' . $this->template . '/js/bootstrap.min.js');
$doc->addScript('templates/' . $this->template . '/js/ie-emulation-modes-warning.js');
$doc->addScript('templates/' . $this->template . 'js/ie10-viewport-bug-workaround.js');
$doc->addScript('templates/' . $this->template . '/js/popper.min.js');


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <jdoc:include type="head"/>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--    css-->
    <link rel="stylesheet"
          href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/template.css">
    <link rel="stylesheet"
          href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/csstemplate.css">
    <link rel="stylesheet"
          href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/bootstrap.css">
    <link rel="stylesheet"
          href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/bootstrap.css.map">
    <link rel="stylesheet"
          href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/bootstrap.min.css.map">
    <link rel="stylesheet"
          href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/bootstrap-grid.css">
    <link rel="stylesheet"
          href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/bootstrap-grid.css.map">
    <link rel="stylesheet"
          href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/bootstrap-grid.min.css">
    <link rel="stylesheet"
          href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/bootstrap-grid.min.css.map">
    <link rel="stylesheet"
          href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/bootstrap-reboot.css">
    <link rel="stylesheet"
          href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/bootstrap-reboot.css.map">
    <link rel="stylesheet"
          href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/bootstrap-reboot.min.css">
    <link rel="stylesheet"
          href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/bootstrap-reboot.min.css.map">
    <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/dizi.css">
    <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/dizi.less">
    <link rel="stylesheet"
          href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/dizi-font.css">
    <link rel="stylesheet"
          href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/dizi-font.min.css">
    <!--    js-->
    <script src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/ie-emulation-modes-warning.js"></script>
    <script src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/ie10-viewport-bug-workaround.js"></script>
    <script src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/popper.min.js"></script>
</head>

<body>
<!-- Header -->
<nav class="navbar navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand float-left" href="#">
            <!--logo-->
            <jdoc:include type="modules" name="logo" style="xhtml"/>
            <!--            <img src="assets/media/dizi-logo.png"></a>-->
            <!-- end logo-->
            <div class="float-right">
                <div class="hamburger-menu d-inline-block">
                    <a href="#" data-toggle="modal" data-target="#navigation"> <span class="icon-hamburger"></span> </a>
                </div>
                <!--            <div class="lang d-inline-block"> <a href="">En</a> </div>-->
            </div>
    </div>
</nav>
<!-- Modal -->
<div class=" navigation modal fade" id="navigation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">

    <!--    navigacija-->
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <ul class="list-unstyled">
                    <li class="py-3">
                        <jdoc:include type="modules" name="mainmenu" style="xhtml"/>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!--    navigacija baigiasi-->
    <!--        <a href="" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true"><span class="icon-close"></span></span>-->
    <!--    </a>-->
    <!--        <div class="modal-dialog modal-sm" role="document">-->
    <!--            <div class="modal-content">-->
    <!--                <div class="modal-body text-center">-->
    <!--                    <ul class="list-unstyled">-->
    <!--                        <li class="py-3"><a href="">Titulinis</a></li>-->
    <!--                        <li class="py-3"><a href="">Paslaugos</a></li>-->
    <!--                        <li class="py-3"><a href="">Projektai</a></li>-->
    <!--                        <li class="py-3 border-none"><a href="">Kontaktai</a></li>-->
    <!--                    </ul>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
</div>

<!-- Content -->
<div class="project">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!--straipsnio pavadinimas-->
                <h1 class="my-4">
                    <jdoc:include type="modules" name="title" style="xhtml"/>
                </h1>
                <!--                pavadinimo pabaiga-->
            </div>
        </div>
        <!--straipsnis-->
        <div class="row mb-5">
            <div class="col-12">
                <jdoc:include type="message"/>
                <jdoc:include type="component"/>

            </div>
        </div>
        <!-- straipsnio pabaiga-->
    </div>
</div>
<!--Footer-->
<footer class="py-4">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-9 d-none d-md-block">
                <a href="mailto:info@dizi.lt" class="email d-block">info@dizi.lt</a>
                <div class="d-inline-block pr-5">
                    <!-- footer kaireje-->
                    <jdoc:include type="modules" name="footer-1" style="xhtml"/>
                    <!-- footer `aireje baigiasi-->
                </div>
                <div class="d-inline-block">
                    <!-- footer centre-->
                    <jdoc:include type="modules" name="footer-2" style="xhtml"/>
                    <!-- footer centre baigiasi-->
                </div>
            </div>
            <div class="col-12 col-md-3 text-md-right text-center">
                <!-- footer desineje-->
                <jdoc:include type="modules" name="footer-3" style="xhtml"/>
                <!--                &copy; Dizi 2001-2017-->
                <!-- footer desineje baigiasi-->
            </div>
        </div>
    </div>
</footer>

</body>

</html>